import copy
import random

import numpy as np
import scipy.linalg


def print_info():
    print("******************************************************************")
    print("*Решение системы линейных алгебраических уравнений методом Гаусса*")
    print("******************************************************************")
    print("*Ax=B                                                            *")
    print("*Размерность n<=20                                               *")
    print("*Размерность и данные можно ввести как в консоли, так и из файла *")
    print("*Формат данных в файле:                                          *")
    print("*/размерность/                                                   *")
    print("*/1 ряд матрицы А/                                               *")
    print("*/2 ряд матрицы А/                                               *")
    print("*/n-ый ряд матрицы А/                                            *")
    print("*/1 ряд матрицы B/                                               *")
    print("*/2 ряд матрицы B/                                               *")
    print("*/n-ый ряд матрицы B/                                            *")
    print("******************************************************************")


def input_n():
    while True:
        n = input("Введите размерность n. N - целое число, лежащее в диапазоне [1; 20]: ")
        if n.isdigit():
            n = int(n)
        else:
            continue
        if (n <= 20) and (n >= 1) and (n - int(n) == 0):
            break
    return n


def input_from_console():
    n = input_n()
    print("Введите матрицу A построчно: ")
    a = list()
    i = 0
    while len(a) < n:
        s = input().split()
        if len(s) == n:
            a.append([float(i) for i in s])
            i += 1
        else:
            print("Количество элементов в строке должно быть равно ", n)
    # matrix_a = [a[i: (i+1)] for i in range(n)]
    # a = matrix_a
    print("Теперь введите вектор B построчно: ")
    b = list()
    i = 0
    while len(b) < n:
        s = input().split()
        if len(s) == 1:
            b.append([float(i) for i in s])
            i += 1
        else:
            print("Количество элементов в строке должно быть равно 1")
    # matrix_b = [b[1*i: 1*(i+1)] for i in range(n)]
    # b = matrix_b
    return a, b, n


def input_from_file():
    path = input("Укажите расположение файла: ")
    f = open(path, 'r')
    n = int(f.readline())
    a = list()
    b = list()
    i = 0
    j = 0
    for line in f:
        if i < n:
            s = line.split()
            if len(s) == n:
                a.append([float(i) for i in s])
                i += 1
            else:
                print("Количество элементов в строке матрицы А должно быть равно указанной размерности")
                return False
        else:
            if j < n:
                s = line.split()
                if len(s) == 1:
                    b.append([float(i) for i in s])
                    j += 1
                else:
                    print("Количество элементов в строке вектора B должно быть равно 1")
                    return False
    return a, b, n


def random_matrix():
    n = input_n()
    a = list()
    b = list()
    for i in range(n):
        a.append(random.sample(range(30), n))
        b.append(random.sample(range(30), 1))
    return a, b, n


def misclosure(a, b, x):
    print("Невязка: \n", np.matmul(np.array(a), np.array(x)) - b)


def gauss(a, b):
    a = copy.deepcopy(a)
    b = copy.deepcopy(b)
    n = len(a)
    p = len(b[0])
    det = 1
    for i in range(n - 1):
        k = i
        for j in range(i + 1, n):
            if abs(a[j][i]) > abs(a[k][i]):
                k = j
        if k != i:
            a[i], a[k] = a[k], a[i]
            b[i], b[k] = b[k], b[i]
            det = -det

        for j in range(i + 1, n):
            t = a[j][i] / a[i][i]
            for k in range(i + 1, n):
                a[j][k] -= t * a[i][k]
            for k in range(p):
                b[j][k] -= t * b[i][k]
    for i in range(n - 1, -1, -1):
        for j in range(i + 1, n):
            t = a[i][j]
            for k in range(p):
                b[i][k] -= t * b[j][k]
        t = 1 / a[i][i]
        det *= a[i][i]
        for j in range(p):
            b[i][j] *= t
    print("Определитель матрицы: ", det)
    print("Решение:\n", b)
    return b


def zeromat(p, q):
    return [[0] * q for i in range(p)]


def matmul(a, b):
    n, p = len(a), len(a[0])
    p1, q = len(b), len(b[0])
    if p != p1:
        raise ValueError("Несовместимые размеры")
    c = zeromat(n, q)
    for i in range(n):
        for j in range(q):
            c[i][j] = sum(a[i][k] * b[k][j] for k in range(p))
    return c


def mapmat(f, a):
    return [list(map(f, v)) for v in a]


def get_solutions(a, b, c, n):
    if np.linalg.matrix_rank(np.array(a).reshape(n, n)) == np.linalg.matrix_rank(c):
        print("Система совместна")
        if np.linalg.matrix_rank(np.array(a).reshape(n, n)) == n:
            print("Система имеет единственное решение")
            ans = gauss(a, b)
            return ans
        else:
            print("Система является неопределенной, то есть имеет бесконечное множество решений")
    else:
        print("Система несовместна. Решений нет.")
        raise Exception


def main():
    n = int()
    print_info()
    command = "continue"
    exist = True
    a = list()
    b = list()
    while command != "exit":
        while True:
            print("\nВведите 1, если хотите ввести данные с клавиатуры")
            print("Введите 2, если хотите ввести данные из файла")
            print("Введите 3, если хотите сгененрировать случайную матрицу")
            s = input("1, 2 или 3? ")
            if s == '1':
                a, b, n = input_from_console()
                break
            if s == '2':
                try:
                    a, b, n = input_from_file()
                except FileNotFoundError:
                    print("Такого файла не существует! ")
                    exist = False
                finally:
                    break
            if s == '3':
                a, b, n = random_matrix()
                break
        if exist:
            npa = np.array(a).reshape(n, n)
            npb = np.array(b).reshape(n, 1)
            p, l, u = scipy.linalg.lu(npa)
            c = np.concatenate((npa, npb), axis=1)
            print("Расширенная матрица:\n", c)
            print("Верхнетреугольная матрица:\n", u)
            try:
                array_x = np.array(np.array(get_solutions(a, b, c, n))).reshape(n, 1)
                misclosure(a, b, array_x)
            except Exception:
                continue
        exist = True
        command = input("Введите exit, если хотите завершить выполнение программы. Enter, чтобы продолжить:\n")


if __name__ == "__main__":
    main()
